package com.innopolis.crmstart.service;

import com.innopolis.crmstart.model.Customer;

import java.util.List;
import java.util.UUID;

public interface CustomerServices {
    Customer getById(UUID id);

    List<Customer> getAll();

    Customer createCustomer(Customer customer);

    Customer updateCustomer(UUID id, Customer customer);

    void delete(UUID id);
}
