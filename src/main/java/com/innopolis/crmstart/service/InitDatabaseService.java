package com.innopolis.crmstart.service;

import com.innopolis.crmstart.model.Customer;
import com.innopolis.crmstart.model.Employee;
import com.innopolis.crmstart.repositories.CustomerRepository;
import com.innopolis.crmstart.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.UUID;

import static com.innopolis.crmstart.CrmStartApplication.ID_OF_MANAGER;

@Service
@RequiredArgsConstructor
public class InitDatabaseService {

    private final EmployeeRepository employeeRepository;
    private final CustomerRepository customerRepository;

    @PostConstruct
    public void init() {

        Employee rop = Employee.builder()
                .id(ID_OF_MANAGER)
                .fullname("РОПин Манагер")
                .telephone("+70000000000")
                .role(Employee.Role.MANAGER)
                .build();
        employeeRepository.save(rop);

        if (employeeRepository.count() < 5) {
            for (int i = 1; i <= 4; i++) {
                Employee employee = Employee.builder()
                        .id(UUID.randomUUID())
                        .fullname("Продавцов Селлер " + i)
                        .telephone("+7111111111" + i)
                        .role(Employee.Role.SELLER)
                        .build();
                employeeRepository.save(employee);
                if (customerRepository.count() < 12) {
                    for (int j = 1; j <= 3; j++) {
                        Customer customer = Customer.builder()
                                .id(UUID.randomUUID())
                                .fullname("Покупской Каст " + i + j)
                                .email("info@company.com")
                                .telephone("+722222222" + i + j)
                                .company("Компания " + j + i)
                                .employee(employee)
                                .build();
                        customerRepository.save(customer);
                    }
                }
            }
        }
    }
}
