package com.innopolis.crmstart.service;

import com.innopolis.crmstart.model.Customer;
import com.innopolis.crmstart.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class CustomerDatabaseServices implements CustomerServices {

    private final CustomerRepository customerRepository;

    @Override
    public List<Customer> getAll() {
        Iterable<Customer> customersIterable = customerRepository.findAll();
        List<Customer> customersList = new ArrayList<>();
        customersIterable.forEach(customersList::add);
        return customersList;
    }

    @Override
    public Customer getById(UUID id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Клиент с id %s не найден", id)));
    }

    @Override
    public Customer createCustomer(Customer customer) {
        customer.setId(UUID.randomUUID());
        return customerRepository.save(customer);
    }

    @Override
    public Customer updateCustomer(UUID id, Customer customer) {
        Customer customerFromDatabase = getById(id);
        Customer customerToUpdate = customerFromDatabase.toBuilder()
                .fullname(customer.getFullname())
                .email(customer.getEmail())
                .telephone(customer.getTelephone())
                .company(customer.getCompany())
                .employee(customer.getEmployee())
                .build();

        return customerRepository.save(customerToUpdate);
    }

    @Override
    public void delete(UUID id) {
        customerRepository.deleteById(id);
    }
}

