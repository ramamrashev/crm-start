package com.innopolis.crmstart.service;

import com.innopolis.crmstart.model.Employee;
import com.innopolis.crmstart.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class EmployeeDatabaseServices implements EmployeeServices {

    private final EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAll() {
        Iterable<Employee> employeeIterable = employeeRepository.findAll();
        List<Employee> employeeList = new ArrayList<>();
        employeeIterable.forEach(employeeList::add);
        return employeeList;
    }

    @Override
    public Employee getById(UUID id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Сотрудник с id %s не найден", id)));
    }

    @Override
    public Employee createEmployee(Employee employee) {
        employee.setId(UUID.randomUUID());
        return employeeRepository.save(employee);
    }

    @Override
    public Employee updateEmployee(UUID id, Employee employee) {
        Employee employeeFromDatabase = getById(id);
        Employee employeeToUpdate = employeeFromDatabase.toBuilder()
                .fullname(employee.getFullname())
                .telephone(employee.getTelephone())
                .role(employee.getRole())
                .build();

        return employeeRepository.save(employeeToUpdate);
    }

    @Override
    public void delete(UUID id) {
        employeeRepository.deleteById(id);
    }
}

