package com.innopolis.crmstart.service;

import com.innopolis.crmstart.model.Employee;

import java.util.List;
import java.util.UUID;

public interface EmployeeServices {
    Employee getById(UUID id);

    List<Employee> getAll();

    Employee createEmployee(Employee customer);

    Employee updateEmployee(UUID id, Employee customer);

    void delete(UUID id);
}
