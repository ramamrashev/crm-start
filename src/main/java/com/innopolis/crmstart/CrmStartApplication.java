package com.innopolis.crmstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.UUID;

@SpringBootApplication
public class CrmStartApplication {
    public static final UUID ID_OF_MANAGER = UUID.fromString("c0f36e0b-e6dd-4b75-9031-e2161a1e95c9");

    public static void main(String[] args) {
        SpringApplication.run(CrmStartApplication.class, args);
    }

}
