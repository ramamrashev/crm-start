package com.innopolis.crmstart.controller;

import com.innopolis.crmstart.model.Customer;
import com.innopolis.crmstart.model.Employee;
import com.innopolis.crmstart.service.CustomerServices;
import com.innopolis.crmstart.service.EmployeeServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/ui/customers")
@RequiredArgsConstructor
public class CustomerFrontendController {

    private final CustomerServices customerService;
    private final EmployeeServices employeeService;

    @GetMapping
    public String customersPage(Model model) {
        List<Customer> customers = customerService.getAll();
        model.addAttribute("customers", customers);
        return "customers/customers";
    }

    @GetMapping("/{id}")
    public String customerPage(Model model, @PathVariable UUID id) {
        Customer customer = customerService.getById(id);
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        model.addAttribute("customer", customer);
        return "customers/customer";
    }

    @GetMapping("/new")
    public String customerCreatePage(Model model) {
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        return "customers/customer_new";
    }

    @PostMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable UUID id) {
        customerService.delete(id);
        return "redirect:/ui/customers";
    }

    @PostMapping
    public String createOrUpdateCustomer(@ModelAttribute("customerDto") Customer customerDto) {
        if (customerDto.getId() == null) {
            customerService.createCustomer(customerDto);
        } else {
            customerService.updateCustomer(customerDto.getId(), customerDto);
        }
        return "redirect:/ui/customers";
    }
}
