package com.innopolis.crmstart.controller;

import com.innopolis.crmstart.model.Customer;
import com.innopolis.crmstart.model.Employee;
import com.innopolis.crmstart.service.CustomerServices;
import com.innopolis.crmstart.service.EmployeeServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("")
@RequiredArgsConstructor
public class HomeFrontendController {

    private final EmployeeServices employeeService;
    private final CustomerServices customerService;
    private List<Employee> employeesList;

    @GetMapping
    public String zeroPage() {
        return "index";
    }

    @GetMapping("/ui")
    public String uiPage() {
        return "home";
    }

    @GetMapping("/ui/home")
    public String homePage() {
        return "home";
    }

    @GetMapping("/index")
    public String indexPage() {
        return "index";
    }

    @GetMapping("/main")
    public String mainPage() {
        return "main";
    }

    @GetMapping("/ui/main")
    public String mainStart(Model model) {
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        return "main";
    }

    @GetMapping("/ui/main/{id}")
    public String mainStart(Model model, @PathVariable UUID id) {
        List<Customer> customersIterable = customerService.getAll();
        List<Customer> customersByEmployee = new ArrayList<>();
        for (Customer customer : customersIterable) {
            UUID idByEmplByCust = customer.getEmployee().getId();
            if (idByEmplByCust.equals(id)) {
                customersByEmployee.add(customer);
            }
        }
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        model.addAttribute("customers", customersByEmployee);
        return "main";
    }
    @GetMapping("/ui/main/list")
    public String mainStartList(Model model, @RequestParam List<Employee> employeesList) {
        this.employeesList = employeesList;
        List<Customer> customersIterable = customerService.getAll();
        List<Customer> customersByEmployee = getCustomersByEmployee(customersIterable);
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        model.addAttribute("employeesWithCustomers", employeesList);
        model.addAttribute("customers", customersByEmployee);
        return "main";
    }

    private List<Customer> getCustomersByEmployee(List<Customer> customersIterable) {
        List<Customer> customersByEmployee = new ArrayList<>();
        for (Customer customer : customersIterable) {
            for (Employee employee : this.employeesList) {
                if (employee.getId().equals(customer.getEmployee().getId())) {
                    customersByEmployee.add(customer);
                    break;
                }
            }
        }
        return customersByEmployee;
    }

    @PostMapping("/ui/main/update")
    public String updateCustomer(@ModelAttribute("customerDto") Customer customerDto) {
        customerService.updateCustomer(customerDto.getId(), customerDto);
        return "redirect:/ui/main/";
    }

    @PostMapping("/ui/main/delete/{id}")
    public String deleteCustomer(@PathVariable UUID id) {
        customerService.delete(id);
        return "redirect:/ui/main/";
    }
}
