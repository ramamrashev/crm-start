package com.innopolis.crmstart.controller;

import com.innopolis.crmstart.model.Customer;
import com.innopolis.crmstart.model.Employee;
import com.innopolis.crmstart.service.CustomerServices;
import com.innopolis.crmstart.service.EmployeeServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.innopolis.crmstart.CrmStartApplication.ID_OF_MANAGER;

@Controller
@RequestMapping("/ui/employees")
@RequiredArgsConstructor
public class EmployeeFrontendController {

    private final EmployeeServices employeeService;
    private final CustomerServices customerService;

    @GetMapping
    public String employeesPage(Model model) {
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        return "employees/employees";
    }

    @GetMapping("/{id}")
    public String employeePage(Model model, @PathVariable UUID id) {
        Employee employee = employeeService.getById(id);
        List<Customer> customersByEmployee = customersByEmployee(id);
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        model.addAttribute("employee", employee);
        model.addAttribute("customers", customersByEmployee);
        return "employees/employee";
    }

    @GetMapping("/new")
    public String employeeCreatePage(Model model) {
        List<Employee> employees = employeeService.getAll();
        model.addAttribute("employees", employees);
        return "employees/employee_new";
    }

    @PostMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable UUID id) {
        List<Customer> customersByEmployee = customersByEmployee(id);
        Employee rop = employeeService.getById(ID_OF_MANAGER);
        for (Customer customer : customersByEmployee) {
            customer.setEmployee(rop);
            customerService.updateCustomer(customer.getId(), customer);
        }
        employeeService.delete(id);
        return "redirect:/ui/employees";
    }

    @PostMapping
    public String createOrUpdateEmployee(@ModelAttribute("employeeDto") Employee employeeDto) {
        if (employeeDto.getId() == null) {
            employeeService.createEmployee(employeeDto);
        } else {
            employeeService.updateEmployee(employeeDto.getId(), employeeDto);
        }
        return "redirect:/ui/employees";
    }

    private List<Customer> customersByEmployee(UUID id) {
        List<Customer> customersIterable = customerService.getAll();
        List<Customer> customersByEmployee = new ArrayList<>();
        for (Customer customer : customersIterable) {
            Employee employeeByCustomer = customer.getEmployee();
            UUID idByEmplByCust = employeeByCustomer.getId();
            if (idByEmplByCust.equals(id)) {
                customersByEmployee.add(customer);
            }
        }
        return customersByEmployee;
    }
}
