package com.innopolis.crmstart.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
public class Employee {
    @Id
    private UUID id;
    private String fullname;
    private String telephone;
    @Enumerated(value = EnumType.STRING)
    private Role role;

    public enum Role {
        MANAGER, SELLER
    }

    public Employee(Employee employee) {
        this.id = employee.getId();
        this.fullname = employee.getFullname();
        this.telephone = employee.getTelephone();
        this.role = employee.getRole();
    }
}
