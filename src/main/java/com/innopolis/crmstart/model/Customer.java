package com.innopolis.crmstart.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
public class Customer {
    @Id
    private UUID id;
    private String fullname;
    private String email;
    private String telephone;
    private String company;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public Customer(Customer customer) {
        this.id = customer.getId();
        this.fullname = customer.getFullname();
        this.email = customer.getEmail();
        this.telephone = customer.getTelephone();
        this.company = customer.getCompany();
        this.employee = customer.getEmployee();
    }
}
