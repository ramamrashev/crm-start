package com.innopolis.crmstart.repositories;

import com.innopolis.crmstart.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface CustomerRepository extends CrudRepository<Customer, UUID> {
}
