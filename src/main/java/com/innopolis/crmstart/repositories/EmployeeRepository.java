package com.innopolis.crmstart.repositories;

import com.innopolis.crmstart.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface EmployeeRepository extends CrudRepository<Employee, UUID> {
}
