package com.innopolis.crmstart;

import com.innopolis.crmstart.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CrmStartApplicationTests {

    @LocalServerPort
    protected int port;

    protected String baseUrl = "http://localhost:";
    protected RestTemplate restTemplate = new RestTemplate();

    @BeforeEach
    public void setUp() {
        baseUrl = baseUrl.concat(port + "");
    }

    @Test
    void contextLoads() {
    }

    protected Employee createTestEmployee() {
        return Employee.builder()
                .id(UUID.randomUUID())
                .fullname("Тестовый Сотрудник")
                .telephone("+70000000000")
                .role(Employee.Role.MANAGER)
                .build();
    }
}