package com.innopolis.crmstart.service;

import com.innopolis.crmstart.CrmStartApplicationTests;
import com.innopolis.crmstart.model.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeDatabaseServicesTest extends CrmStartApplicationTests {

    @Autowired
    private EmployeeDatabaseServices service;

    @Test
    void getAll() {
        List<Employee> employeesList = service.getAll();
        assertNotNull(employeesList);
    }

    @Test
    void getById() {
        Employee testEmployee = service.createEmployee(createTestEmployee());

        Employee employee = service.getById(testEmployee.getId());

        assertNotNull(employee);
        assertEquals(employee.getId(), testEmployee.getId());
        assertEquals(employee.getFullname(), testEmployee.getFullname());
        assertEquals(employee.getTelephone(), testEmployee.getTelephone());
        assertEquals(employee.getRole(), testEmployee.getRole());
    }

    @Test
    void createEmployee() {
        Employee newEmployee = createTestEmployee();

        Employee createdEmployee = service.createEmployee(newEmployee);

        assertNotNull(createdEmployee.getId());
        assertEquals(newEmployee.getFullname(), createdEmployee.getFullname());
        assertEquals(newEmployee.getTelephone(), createdEmployee.getTelephone());
        assertEquals(newEmployee.getRole(), createdEmployee.getRole());
    }

    @Test
    void updateEmployee() {
        Employee newEmployee = service.createEmployee(createTestEmployee());

        Employee newDateOfEmployee = new Employee(newEmployee).toBuilder()
                .id(UUID.randomUUID())
                .fullname("Улучшенный Сотрудник")
                .telephone("+77777777777")
                .role(Employee.Role.SELLER)
                .build();

        Employee updatedEmployee = service.updateEmployee(newEmployee.getId(), newDateOfEmployee);

        assertEquals(newEmployee.getId(), updatedEmployee.getId(), "Идентификаторы не должны меняться во время обновления");
        assertNotEquals(updatedEmployee.getFullname(), newEmployee.getFullname());
        assertNotEquals(updatedEmployee.getTelephone(), newEmployee.getTelephone());
        assertNotEquals(updatedEmployee.getRole(), newEmployee.getRole());
    }

    @Test
    void delete() {
        Employee newEmployee = service.createEmployee(createTestEmployee());
        
        UUID newEmployeeId = newEmployee.getId();
        service.delete(newEmployeeId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> service.getById(newEmployeeId),
                format("Ожидается, что ничего не вернет и выдаст исключение, поскольку сотрудник с id %s был удален",
                        newEmployeeId)
        );

        assertEquals(format("404 NOT_FOUND \"Сотрудник с id %s не найден\"", newEmployeeId), exception.getMessage());
    }
}