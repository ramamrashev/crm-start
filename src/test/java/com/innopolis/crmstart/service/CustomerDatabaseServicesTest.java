package com.innopolis.crmstart.service;

import com.innopolis.crmstart.CrmStartApplicationTests;
import com.innopolis.crmstart.model.Customer;
import com.innopolis.crmstart.model.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class CustomerDatabaseServicesTest extends CrmStartApplicationTests {

    @Autowired
    private CustomerDatabaseServices customerService;

    @Autowired
    private EmployeeDatabaseServices employeeService;

    @Test
    void getAll() {
        List<Customer> customersList = customerService.getAll();
        assertNotNull(customersList);
    }

    @Test
    void getById() {
        Customer testCustomer = customerService.createCustomer(createTestCustomer());
        UUID testCustomerId = testCustomer.getId();
        Customer customer = customerService.getById(testCustomerId);
        assertNotNull(customer);
        assertEquals(customer.getId(), testCustomerId);
        assertEquals(customer.getFullname(), testCustomer.getFullname());
        assertEquals(customer.getEmail(), testCustomer.getEmail());
        assertEquals(customer.getTelephone(), testCustomer.getTelephone());
        assertEquals(customer.getCompany(), testCustomer.getCompany());
        assertEquals(customer.getEmployee().getId(), testCustomer.getEmployee().getId());
    }

    @Test
    void createCustomer() {
        Customer newCustomer = createTestCustomer();

        Customer createdCustomer = customerService.createCustomer(newCustomer);

        assertNotNull(createdCustomer);
        assertNotNull(createdCustomer.getFullname());
        assertNotNull(createdCustomer.getEmail());
        assertNotNull(createdCustomer.getTelephone());
        assertNotNull(createdCustomer.getCompany());
        assertNotNull(createdCustomer.getEmployee());
    }

    @Test
    void updateCustomer() {
        Customer newCustomer = customerService.createCustomer(createTestCustomer());
        UUID newCustomerId = newCustomer.getId();
        Employee testEmployee = employeeService.createEmployee(createTestEmployee());
        Customer newDataOfCustomer = new Customer(newCustomer).toBuilder()
                .fullname("Улучшенный Клиент")
                .email("upgrade@test.test")
                .telephone("+77777777777")
                .company("Улучшенная Компания")
                .employee(testEmployee)
                .build();

        Customer updatedCustomer = customerService.updateCustomer(newCustomerId, newDataOfCustomer);

        assertEquals(newCustomerId, updatedCustomer.getId(), "Идентификаторы не должны меняться во время обновления");
        assertNotEquals(updatedCustomer.getFullname(), newCustomer.getFullname());
        assertNotEquals(updatedCustomer.getEmail(), newCustomer.getEmail());
        assertNotEquals(updatedCustomer.getTelephone(), newCustomer.getTelephone());
        assertNotEquals(updatedCustomer.getCompany(), newCustomer.getCompany());
        assertNotEquals(updatedCustomer.getEmployee(), newCustomer.getEmployee());
    }

    @Test
    void delete() {
        Customer newCustomer = customerService.createCustomer(createTestCustomer());

        UUID newCustomerId = newCustomer.getId();
        customerService.delete(newCustomerId);

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> customerService.getById(newCustomerId),
                format("Ожидается, что ничего не вернет и выдаст исключение, поскольку клиент с id %s был удален",
                        newCustomerId)
        );

        assertEquals(format("404 NOT_FOUND \"Клиент с id %s не найден\"", newCustomerId), exception.getMessage());
    }

    private Customer createTestCustomer() {
        Employee testEmployee = employeeService.createEmployee(createTestEmployee());
        return Customer.builder()
                .fullname("Тестовый Клиент")
                .email("test@test.test")
                .telephone("+70000000000")
                .company("Тестовая Компания")
                .employee(testEmployee)
                .build();
    }
}